# Development Box

This Box contains some development tools.

- git-hub #  Git command line interface to GitHub
- git-extras # Awesome Git utilities
- htop # Interactive process viewer
- jq # Command line JSON processor
- liquidprompt # A full-featured & carefully designed adaptive prompt for bash & zsh
- mc # MidnightCommander - visual shell for Unix-like systems
- mlocate # Find files by name
- multitail # Browse through several files at once
- mutt # The Mutt Mail User Agent
- ncdu # NCurses Disk Usage
- mbuffer # Buffers  I/O  operations and displays the throughput rate
- pandoc # General markup converter
- pv # Monitor the progress of data through a pipe
- tig # Text-mode interface for Git
- silversearcher-ag # The Silver Searcher. Like ack, but faster
- tofrodos # Convert text files "todos", "fromdos", "dos2unix" and "unix2dos"
- tree # List contents of directories in a tree-like format
- vim # Vi IMproved, a programmers text editor
- w3m # A text based web browser and pager
- wget # The non-interactive network downloader
- xml2 # Convert xml documents in a flat format
- xmlstarlet # Command line XML/XSLT toolkit

Requirements
* [Git](https://git-scm.com/downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## Installation

Terminal (Linux/macOS) or GitBash (Windows)
```
git clone https://gitlab.com/axel-klinger/groovy-box.git
cd groovy-box
vagrant up
```

Connect with ssh
```
vagrant ssh
```
